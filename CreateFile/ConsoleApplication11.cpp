#undef UNICODE
#include <windows.h>

#include <iostream>
using namespace std;

int main()
{
    HANDLE h = CreateFile
       ("C:\\temp\\abc.txt", // lpFileName
        GENERIC_READ | GENERIC_WRITE, // dwDesiredAccess
        0, // dwShareMode
        NULL, // lpSecurityAttributes
        CREATE_ALWAYS, // dwCreationDisposition,
        FILE_ATTRIBUTE_NORMAL, // dwFlagsAndAttributes,
        NULL); // hTemplateFile

    if (h == INVALID_HANDLE_VALUE)
    {
        // cout << "Nepovedlo se " << GetLastError () << endl;
        int code = GetLastError();
        cout << "Error code: " << code << endl;

        char* answer = (char*)"nic";

        FormatMessage
        (
            FORMAT_MESSAGE_ALLOCATE_BUFFER |
            FORMAT_MESSAGE_FROM_SYSTEM /* |
            FORMAT_MESSAGE_IGNORE_INSERTS */, // DWORD dwFlags

            NULL, // LPCVOID lpSource,
            code, // DWORD dwMessageId,
            0, // DWORD dwLanguageId,
            (LPTSTR)&answer, //  LPTSTR lpBuffer,
            0, // DWORD nSize,
            NULL // va_list* Arguments
        );

        cout << "Error: " << answer << endl;
        LocalFree (answer);
    }
    else
    {
        cerr << "Otevreno" << endl;

        char buf[] = "Nejaky text";
        DWORD result;
        WriteFile(h, buf, strlen(buf), &result, NULL);

        CloseHandle (h);
    }
}

